#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <sstream>
#include <fstream>
#include <sys/resource.h>

#define MAXPATH 200
#define MAXLINE 10000
#define MAXPARLEN 20
#define MAXPAR 20

using namespace std;

struct rlimit r;	//pipe_limit
struct pipe_cmd	//PTable
{
	int num;
	char * cmd[MAXPAR];
};
struct waiting_pipe	//WTable
{
	int num;
	int fd[2];
};
void initialize_path()	//Initialize the program directory
{
	char pro_path[MAXPATH];
	getcwd(pro_path,MAXPATH);	//get current directory path
	strcat(pro_path,"/ras/");	//add "/ras/"
	chdir(pro_path); 	//initialize the program directory
	chroot(pro_path);	//initialize defalt root PATH
	setenv("PATH","bin:.",1);	//initial PATH variable is bin/ and ./
}
void send_welcome_msg(int sockfd)
{
	char welcome_msg[300];
	strcpy(welcome_msg,"****************************************\n");
	strcat(welcome_msg,"** Welcome to the information server. **\n");
	strcat(welcome_msg,"****************************************\n");
	//write(socket,char* msg,strlen(msg));
	if(write(sockfd,welcome_msg,strlen(welcome_msg))<0)
	{cout<<"Send welcome_msg error..."<<endl;}
	else
	{cout<<"Send welcome_msg correct..."<<endl;}
	memset(welcome_msg,0,sizeof(welcome_msg));	//clean every time	
}
void send_symbol(int sockfd)
{
	char symbol[3];
	strcpy(symbol,"% ");
	//write(socket,char* msg,strlen(msg));
	write(sockfd,symbol,strlen(symbol));
	memset(symbol,0,sizeof(symbol));	//clean every time
}
int readline(int fd, char * ptr,int maxlen)
{
  int n,rc;
  char c;
  for(n=1;n<maxlen;n++)
  {
    if ((rc=read(fd,&c,1))==1)
    {
      *ptr++=c;
      if(c=='\n'){break;}
    }
    else if(rc==0)
    {
      if(n==1){return(0);}  //EOF, no data read
      else {break;} //EOF, some data was read
    }
    else
    {return(-1);}
  }
  *ptr=0;
  return(n);	//return length
}
void set_pipe()
{
	if(getrlimit(RLIMIT_NOFILE,&r)<0)
	{
		fprintf(stderr,"getrlimit error\n");
		exit(1);
	}
	printf("RLIMIT_NOFILE cur:%d\n",r.rlim_cur);
	printf("RLIMIT_NOFILE max:%d\n",r.rlim_max);

	/** set limit **/
	r.rlim_cur = 2200;
	r.rlim_max = 3000;
	if (setrlimit(RLIMIT_NOFILE,&r)<0)
	{
		fprintf(stderr,"setrlimit error\n");
		exit(1);
	}
}

int main(int argc, char *argv[])
{
	set_pipe();
	initialize_path();
	
	int port = atoi(argv[1]);	//port number	
	int sockfd,newsockfd,clilen,childpid;
  	struct sockaddr_in cli_addr,serv_addr;
  	/////////////Open a TCP socket (ppt p.7)///////////////
	if ((sockfd=socket(AF_INET,SOCK_STREAM,0))<0)
	{cout<<"server: can't open stream socket."<<endl;}
	////////////////////Bind (ppt p.8)////////////////////
	bzero((char *)&serv_addr,sizeof(serv_addr));
	serv_addr.sin_family=AF_INET;
	serv_addr.sin_addr.s_addr=htonl(INADDR_ANY);
	serv_addr.sin_port=htons(port);

	//port reuse
  	int enable=1;
  	setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&enable,sizeof(int));

  	if(bind(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr))<0)
	{cout<<"server: can't bind local address."<<endl;}
	////////////////////Listen (ppt p.8)////////////////////
	listen(sockfd,5);	//backlog=5, the waiting amount in queue
	cout<<"Server is opened..."<<endl;

	////////////////////Accept (ppt p.9)////////////////////
  	while(1)
  	{
	    //accept client (newsockfd)
	    clilen=sizeof(cli_addr);
	    newsockfd=accept(sockfd,(struct sockaddr *)&cli_addr,(socklen_t*)&clilen);  //(socklen_t*)
	    if(newsockfd<0) {cout<<"server: accept error..."<<endl;}
	    if((childpid=fork())<0) {cout<<"server: fork error..."<<endl;}
	    else if(childpid==0)	//child process of main: Accept correct
	    {
	    	close(sockfd);	//close original socket
	    	send_welcome_msg(newsockfd);
	    	///////////////Create WTable///////////////
	    	waiting_pipe WTable[5000];
	    	int waiting_counter=0;
	    	int unknowncmd_flag=0;	//check close or not
			int n;
			char line[MAXLINE];	//MAX input lenth:10000 char
			stringstream ss;
			/////////////repeat to readline from client/////////////
			while(1)
			{
				send_symbol(newsockfd);

				n=readline(newsockfd,line,MAXLINE);
				string line_str=line;				
				
				char cli_cmd_par[MAXPARLEN];	//each parameter(20 char)
				pipe_cmd PTable[3000];	//MAX command amount for each line
				for(int i=0;i<3000;i++)
				{
					//PTable[i].cmd=new char * [MAXPAR];	//new 1st dimension:MAXPAR
					for(int j=0;j<MAXPAR;j++)
					{
						PTable[i].cmd[j]=new char [MAXPARLEN];	//new 2nd dimension:MAXPARLEN
						//PTable[i].cmd[j]=(char*)malloc(sizeof(char)*MAXPARLEN);
						memset(PTable[i].cmd[j],0,MAXPARLEN);//Initial set null
					}
				}
				int pipe_counter=0;
				int cmd_counter=0;
				int end_flag=0; //record EOF: cmd or |num
				/////////////Save input to PTable/////////////
				ss<<line_str;
				while(ss>>cli_cmd_par)	//Split with whitespace
				{
					if(cli_cmd_par[0]!='|')	//cmd
					{
						PTable[pipe_counter].num=0;	//set defalt
						for(int i=0;i<MAXPARLEN;i++)	//copy by value (each char)
						{
							PTable[pipe_counter].cmd[cmd_counter][i]=cli_cmd_par[i];
						}
						cmd_counter++;
						end_flag=0;
					}
					else	//| or |num
					{
						string str=cli_cmd_par;
						if(str.length()!=1)	//|num
						{
							int num=0;
							for(int i=1;i<str.length();i++)
							{
								if(isdigit(str[i]))
								{
								  num*=10;
								  num+=str[i]-'0';
								}
							}
							PTable[pipe_counter].num=num;
							PTable[pipe_counter].cmd[cmd_counter]=NULL;
							cmd_counter=0;
							pipe_counter++;
							end_flag=1;
						}
						else	//only |
						{
							PTable[pipe_counter].num=1;
							PTable[pipe_counter].cmd[cmd_counter]=NULL;
							cmd_counter=0;
							pipe_counter++;
						}
					}
					//clean every time
					memset(cli_cmd_par,0,sizeof(cli_cmd_par));
				}
				ss.str("");	//Clean SS
				ss.clear();
				memset(line,0,sizeof(line));	//clean after saved (client input)
				if (end_flag==0)	//END is cmd (need add '\0')
				{
					PTable[pipe_counter].cmd[cmd_counter]=NULL;
					pipe_counter++;
				}
				///////////////////Print PTable///////////////////
				cout<<"=========New readline========="<<endl;
				cout<<"Pipe counter:"<<pipe_counter<<endl;
				for(int i=0;i<pipe_counter;i++)
				{
					cout<<"~~~Pipe cmd "<<i<<"~~~num="<<PTable[i].num<<endl;
					for(int j=0;j<MAXPAR;j++)
					{
						if(PTable[i].cmd[j]!=NULL)
						{
							cout<<PTable[i].cmd[j]<<"\t";
						}
						else{break;}
					}
					cout<<endl;
				}
				//exit
				if(pipe_counter==1 && strcmp(PTable[0].cmd[0],"exit")==0)
				{
					cout<<"Client exit!"<<endl;
					exit(1);	//close child
				}
				//setenv
				else if(pipe_counter==1 && strcmp(PTable[0].cmd[0],"setenv")==0)
				{
					setenv(PTable[0].cmd[1],PTable[0].cmd[2],1);
					//setenv(name,value,1), 1 means overwrite
				}
				//printenv
				else if(pipe_counter==1 && strcmp(PTable[0].cmd[0],"printenv")==0)
				{
					char result[60];
					strcpy(result,PTable[0].cmd[1]);
					strcat(result,"=");
					strcat(result,getenv(PTable[0].cmd[1]));
					strcat(result,"\n");
					//write(socket,char* msg,strlen(msg));
					write(newsockfd,result,strlen(result));
					memset(result,0,sizeof(result));	//clean every time
				}
				//cmd and pipe
				else
				{
					char filename[30];
					int file_out=0;	//need to output file or not
					for(int i=0;i<pipe_counter;i++)
					{
						cout<<"=========["<<i<<"]========="<<endl;
						int fd_in=STDIN_FILENO;
						int fd_out=STDOUT_FILENO;
						int in_index,out_index;
						//(0)Check filename first
						for(int j=0;j<MAXPAR;j++)
						{
							if(PTable[i].cmd[j]==NULL){break;}
							else
							{
								if(strcmp(PTable[i].cmd[j],">")==0)
								{
									file_out=1;
									strcpy(filename,PTable[i].cmd[j+1]);
									PTable[i].cmd[j]=NULL;
									PTable[i].cmd[j+1]=NULL;
									break;
								}
							}
						}
						///////Check num and create pipe to WTable///////
						//(1)All minus 1
						for(int j=0;j<waiting_counter;j++)
						{
							WTable[j].num--;
						}
						//(2)Check same or create pipe (stdout) fd_out,out_index
						if(PTable[i].num!=0)
						{
							int tmp=0;
							for(int j=0;j<waiting_counter;j++)
							{
								//same pipe num
								if(WTable[j].num==PTable[i].num)
								{
									fd_out=WTable[j].fd[1];
									out_index=j;
									tmp=1;
									break;
								}
							}
							if(tmp==0)	//Not found same, create
							{
								cout<<"[new]"<<endl;
				                WTable[waiting_counter].num=PTable[i].num;
				                pipe(WTable[waiting_counter].fd);
				                fd_out=WTable[waiting_counter].fd[1];
				                out_index=waiting_counter;
				                waiting_counter++;
							}
						}
						cout<<"[WTable] "<<waiting_counter<<endl;
						for(int j=0;j<waiting_counter;j++)
						{
							if(WTable[j].num==0)
							{
								cout<<j<<" *\t"<<WTable[j].num<<endl;;
							}
							else
							{
								cout<<j<<"\t"<<WTable[j].num<<endl;
							}
						}
						if(fd_out!=STDOUT_FILENO){cout<<"@@@@@@"<<endl;}
						else{cout<<"STDOUT"<<endl;}

						//(3)Check zero (stdin)
						for(int j=0;j<waiting_counter;j++)
						{
							if(WTable[j].num==0)
							{
								in_index=j;
								fd_in=WTable[j].fd[0];
								//close(WTable[j].fd[1]);
								break;
							}
						}
						if(fd_in!=STDIN_FILENO){cout<<"######"<<endl;}
						else{cout<<"STDIN"<<endl;}
						//(4)exec
						int status;
						pid_t cmd_childpid=fork();
			            if(cmd_childpid<0)
			            {
							perror("fork_pipe");
							exit(1);
			            }
			            //child
			            else if(cmd_childpid==0)
			            {
			            	//not last
			            	if(i!=pipe_counter-1)
			            	{
			            		if(unknowncmd_flag==0)
				            	{
				            		if(fd_in!=STDIN_FILENO){close(WTable[in_index].fd[1]);}
				            	}
			            		dup2(fd_in,0);
			            		if(fd_out!=STDOUT_FILENO){close(WTable[out_index].fd[0]);}
			            		dup2(fd_out,1);
			            		dup2(newsockfd,2);
			            		if(execvp(PTable[i].cmd[0],PTable[i].cmd)!=0)
				                {
				                	char unknown_msg[]="Unknown command: [";
					                strcat(unknown_msg,PTable[i].cmd[0]);
					                strcat(unknown_msg,"].\n");
					                write(newsockfd,unknown_msg,strlen(unknown_msg));
					                memset(unknown_msg,0,sizeof(unknown_msg));
					                if(fd_in!=STDIN_FILENO){close(fd_in);}
				                	close(fd_out);	//must
					                exit(3);
				                }
				                else
				                {
				                	if(fd_in!=STDIN_FILENO){close(fd_in);}
				                	close(fd_out);	//must
				                	exit(1);
				                }
			            	}
			            	//last: file, |num, socket
			            	else
			            	{
			            		if(unknowncmd_flag==0)
				            	{
				            		if(fd_in!=STDIN_FILENO){close(WTable[in_index].fd[1]);}
				            	}
			            		dup2(fd_in,0);
			            		dup2(newsockfd,2);
			            		//output to file
			            		if(file_out==1)
			            		{
			            			freopen(filename,"w",stdout);
			            			if(execvp(PTable[i].cmd[0],PTable[i].cmd)!=0)
					                {
										char unknown_msg[]="Unknown command: [";
						                strcat(unknown_msg,PTable[i].cmd[0]);
						                strcat(unknown_msg,"].\n");
						                write(newsockfd,unknown_msg,strlen(unknown_msg));
						                memset(unknown_msg,0,sizeof(unknown_msg));
						                fclose(stdout);
					                	if(fd_in!=STDIN_FILENO){close(fd_in);}
					                	exit(3);
					                }
					                else
					                {
					                	fclose(stdout);
					                	if(fd_in!=STDIN_FILENO){close(fd_in);}
					                	exit(1);
					                }
			            		}	//end of file_out
			            		//output to |num
			            		else if(PTable[i].num!=0)
			            		{
			            			if(fd_out!=STDOUT_FILENO){close(WTable[out_index].fd[0]);}
			            			dup2(fd_out,1);
			            			if(execvp(PTable[i].cmd[0],PTable[i].cmd)!=0)
					                {
										char unknown_msg[]="Unknown command: [";
						                strcat(unknown_msg,PTable[i].cmd[0]);
						                strcat(unknown_msg,"].\n");
						                write(newsockfd,unknown_msg,strlen(unknown_msg));
						                memset(unknown_msg,0,sizeof(unknown_msg));
						                if(fd_in!=STDIN_FILENO){close(fd_in);}
				                		close(fd_out);	//must
				                		exit(3);
					                }
					                else
					                {
					                	if(fd_in!=STDIN_FILENO){close(fd_in);}
				                		close(fd_out);
				                		exit(1);
					                }
			            		}	//end of |num
			            		//output to socket (cmd)
			            		else
			            		{
			            			dup2(newsockfd,1);
			            			if(execvp(PTable[i].cmd[0],PTable[i].cmd)!=0)
					                {
					                	char unknown_msg[]="Unknown command: [";
						                strcat(unknown_msg,PTable[i].cmd[0]);
						                strcat(unknown_msg,"].\n");
						                write(newsockfd,unknown_msg,strlen(unknown_msg));
						                memset(unknown_msg,0,sizeof(unknown_msg));
						                if(fd_in!=STDIN_FILENO){close(fd_in);}
						                exit(3);
					                }
					                else
					                {
					                	if(fd_in!=STDIN_FILENO){close(fd_in);}
					                	exit(1);
					                }
			            		}	//end of socket
			            	}	//end of last
			            }	//end of child
			            //parent
			            else
			            {
			            	if(unknowncmd_flag==0)
			            	{
			            		if(fd_in!=STDIN_FILENO){close(WTable[in_index].fd[1]);}
			            	}
			            	else	//if have unknowncmd, not close again.
			            	{
			            		unknowncmd_flag=0;
			            	}
					        waitpid(cmd_childpid,&status,0);
					        cout<<"STATUS:"<<WEXITSTATUS(status)<<endl;
					        if(WEXITSTATUS(status)==0)	//correct cmd
					        {
					        	if(fd_in!=STDIN_FILENO)
					        	{
					        		close(fd_in);
					        	}
					        }
					        if(WEXITSTATUS(status)==3)	//error cmd
					        {
					        	if(i!=0)
				                {
				                	unknowncmd_flag=1;	//have unknowncmd
				                	for(int j=0;j<waiting_counter;j++)
									{
										WTable[j].num++;
									}
									i=pipe_counter-1;	//move to last one
				                }
					        }
			            }	//end of parent
						//(5)update WTable
						for(int j=0;j<waiting_counter;j++)
						{
							if(WTable[j].num==0)	//move all
							{
								//waiting_couter is Bound
								for(int k=j+1;k<waiting_counter;k++)
								{
									WTable[k-1].num=WTable[k].num;
									WTable[k-1].fd[0]=WTable[k].fd[0];
                 					WTable[k-1].fd[1]=WTable[k].fd[1];
								}
								waiting_counter--;
								break;	//only one zero
							}
						}
						cout<<"[Update WTable] "<<waiting_counter<<endl;
						for(int j=0;j<waiting_counter;j++)
						{
							cout<<j<<"\t"<<WTable[j].num<<endl;
						}

					}	//end of for loop (PTable all cmd)
				}	//end of if (cmd and pipe)					
				

				///////////////////Delete PTable///////////////////
				//Delete PTable.cmd, delete 2 dimension
				for(int i=0;i<3000;i++)
				{				
					for(int j=0;j<MAXPAR;j++)	//20
					{
						delete [] PTable[i].cmd[j];	//delete 2nd dimension:MAXPARLEN
					}
					//delete [] PTable[i].cmd;	//delete 1st dimension:MAXPAR
				}
				//Echo
        		/*if(write(newsockfd,line,n)!=n)	//length not correct
        		{cout<<"Echo: writen error"<<endl;}*/
        		
			}	//end of readline from client

	    	exit(0);

	    }	//end of child

	    close(newsockfd);	//parent process of main
	    wait(NULL);	//kill zombie----change:SIGNAL
	}	//end of while(1)

	return 0;
}